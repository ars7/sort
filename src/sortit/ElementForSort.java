package sortit;

import java.io.File;
import java.util.List;

import sortit.impl.FileEditorImpl;
import sortit.impl.SorterImpl;
import sortit.interfaces.IFileEditor;
import sortit.interfaces.ISorter;

public class ElementForSort implements Runnable{
	/**
	 * @author Arsalan Elanov
	 * 
	 */
	
	private File file;
	private SortingInstruction instruction;
	
	public ElementForSort(File file, SortingInstruction instruction) {
		this.file = file;
		this.instruction = instruction;
	}

	@Override
	public void run() {
		IFileEditor fileEditor = new FileEditorImpl();
		
		try {
			
			//getting items from file
			List<Object> list = fileEditor.listItemsFromFile(file, this.instruction.getContentType());
			
			//sorting
			ISorter sorter = new SorterImpl();
			sorter.sortWithChangeList(list, this.instruction.getContentType(), this.instruction.getSortingDirection(), file.getName());
			
			//write to file
			fileEditor.writeInFile(list, this.instruction.getPath(), this.instruction.getOutPrefixToFile(), this.file.getName());
			
		}catch (InterruptedException e) {
			System.out.println(e.getMessage()); 
			
			return;
			
		}catch(Exception e) {
			if(e.getMessage() == null)
				System.out.println("Error when working with file " + file.getName() + ". " + e.getMessage());
			
			else 
				System.out.println(e.getMessage()); 
			
			return;
		}
		
		System.out.println("File " + this.file.getName() + " successfully sorted!");
		
	}
	
	

}
