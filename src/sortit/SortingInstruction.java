package sortit;

import sortit.enums.ContentType;
import sortit.enums.SortingDirection;

public class SortingInstruction {
	/**
	 * @author Arsalan Elanov
	 */
	
	private String path; //path to the directory with input files
	private String outPrefixToFile; //prefix to generated files
	private ContentType contentType; //item type in input files
	private SortingDirection sortingDirection; // asc or desc
	
	
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getOutPrefixToFile() {
		return outPrefixToFile;
	}
	public void setOutPrefixToFile(String outPrefixToFile) {
		this.outPrefixToFile = outPrefixToFile;
	}
	public ContentType getContentType() {
		return contentType;
	}
	public void setContentType(ContentType contentType) {
		this.contentType = contentType;
	}
	public SortingDirection getSortingDirection() {
		return sortingDirection;
	}
	public void setSortingDirection(SortingDirection sortingDirection) {
		this.sortingDirection = sortingDirection;
	}
	
}
