package sortit.impl;

import java.util.List;

import sortit.enums.ContentType;
import sortit.enums.SortingDirection;
import sortit.interfaces.ISorter;

public class SorterImpl implements ISorter {
	/**
	 * @author Arsalan Elanov
	 */
	
	/**
	 * <p>Sort by changing the input list of items</p>
	 * 
	 * @param list the list of items that you want to sort
	 * @param contentType estimated item type
	 * @param sortingDirection  (asc or desc)
	 * 
	 * @exception ClassCastException if the types of the elements of the 
	 * list are not consistent with the intended type (contentType)
	 */
	public void sortWithChangeList(List<Object> list, ContentType contentType, SortingDirection sortingDirection, String fileName) throws ClassCastException, InterruptedException {
		
		for(int i = 1; i < list.size(); i++) {
			int j = i - 1;
			
			Object currentElement = list.get(i); 
			
			while(j >= 0 
					&& this.getCondition(currentElement, 
							list.get(j), 
							contentType, 
							sortingDirection)) 
			{
				j--;
			}
			
			j++;
			
			list.remove(i);
			list.add(j,currentElement);
				
			if(Thread.currentThread().isInterrupted()) {
				throw new InterruptedException("Sort file " + fileName + " interrupted");
				
			}
		}
		
	}
	
	/**
	 * <p>Comparing elements with specified parameters</p>
	 * @param firstElement 
	 * @param secondElement 
	 * @param contentType estimated item type
	 * @param sortingDirection (asc or desc)
	 * @return
	 * @throws ClassCastException at mismatch of types of elements (firstElement, secondElement) 
	 * with the intended type (contentType)
	 */
	private Boolean getCondition(Object firstElement, Object secondElement, ContentType contentType, SortingDirection sortingDirection) throws ClassCastException {
		
		if(contentType == ContentType.INTEGER) {
			Integer firstElementInt = (Integer) firstElement;
			Integer secondElementInt = (Integer) secondElement;
			
			if(sortingDirection == SortingDirection.ASC) { 
				return firstElementInt.intValue() < secondElementInt.intValue();
				
			} else {
				return firstElementInt.intValue() > secondElementInt.intValue();
			}
			
		} else if(contentType == ContentType.STRING){
			
			String firstElementStr = (String) firstElement;
			String secondElementStr = (String) secondElement;
			
			if(sortingDirection == SortingDirection.ASC) {
				if(firstElementStr.compareTo(secondElementStr) < 0) 
					return true;
				else 
					return false;
				
			} else {
				if(firstElementStr.compareTo(secondElementStr) > 0) 
					return true;
				else 
					return false;
			}
		}
		
		return null;
		
	}
}
