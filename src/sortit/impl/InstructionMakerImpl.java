package sortit.impl;

import sortit.SortingInstruction;
import sortit.enums.ContentType;
import sortit.enums.SortingDirection;
import sortit.interfaces.IInstructionMaker;

public class InstructionMakerImpl implements IInstructionMaker{
	/**
	 * @author Arsalan Elanov
	 */
	
	private final String prefixInstruction = "--out-prefix=";
	private final String contentTypeInstruction = "--content-type=";
	private final String sortModeInstruction = "--sort-mode=";

	/**
	 * <p>Getting instructions ( class SortingInstruction) to sort and create files</p>
	 * @param args array of input parameters
	 * 
	 * @exception IllegalArgumentException if the parameter name is incorrect
	 */
	
	public SortingInstruction getInstruction(String[] args) throws IllegalArgumentException {
		
		SortingInstruction instruction = new SortingInstruction();
		try {
		
			//file path
			instruction.setPath(args[0]);
			
			//file name prefix
			if(args[1].startsWith(prefixInstruction)) {
				instruction.setOutPrefixToFile(args[1].substring(prefixInstruction.length(), args[1].length()));
			}
			
			//strings or numbers
			if(args[2].equalsIgnoreCase(contentTypeInstruction + "i")) {
				instruction.setContentType(ContentType.INTEGER); 
				
			} else if(args[2].equalsIgnoreCase(contentTypeInstruction + "s")) {
				instruction.setContentType(ContentType.STRING);
			}
			
			//sort asc or desc
			if(args[3].equalsIgnoreCase(sortModeInstruction + "a")) {
				instruction.setSortingDirection(SortingDirection.ASC);
				
			} else if(args[3].equalsIgnoreCase(sortModeInstruction + "d")) {
				instruction.setSortingDirection(SortingDirection.DESC);
				
			}
		
		}catch(Exception e) {
			throw new IllegalArgumentException("Error in input parameters!");
			
		} finally {
			if(instruction.getPath() == null 
					|| instruction.getOutPrefixToFile() == null 
					|| instruction.getSortingDirection() == null 
					|| instruction.getSortingDirection() == null) {
				
				throw new IllegalArgumentException("Error in input parameters!");
			}
		}
		
		return instruction;
		
		}

}
