package sortit.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import sortit.enums.ContentType;
import sortit.interfaces.IFileEditor;


public class FileEditorImpl implements IFileEditor {
	/**
	 * @author �rsalan Elanov
	 */
	
	/**
	 * <p>Getting a list of files from a given directory</p>
	 * 
	 * @param fileDirectory directory path
	 * @value array of elements of type File
	 * 
	 * @exception IOException If the path to the directory is specified incorrectly, IOException is possible.
	 */
	
	public File[] getFileElements(String fileDirectory) throws IOException{
		
		try {
			File file = new File(fileDirectory);
			File[] files = file.listFiles();
			
			return files;
			
		}catch (Exception e) {
			throw new IOException("Directory Error " + fileDirectory);
		}
		
	}
	
	/**
	 * <p>Getting a list of items from a given file</p>
	 * 
	 * @param file file with items
	 * @param contentType estimated item type
	 * 
	 * @return returns list (LinkedList) with elements from file
	 * 
	 * @exception IOException in case of file reading error
	 * @exception InterruptedException upon forced termination of this thread
	 * @exception IllegalArgumentException if the types of elements in the file with the expected type do not match
	 */
	public List<Object> listItemsFromFile(File file, ContentType contentType) throws IOException, IllegalArgumentException, InterruptedException{
		
		List<Object> list= new LinkedList<>();
		
		FileReader fr = null;
		Scanner scanner = null;
				
		try {
			
			fr = new FileReader(file);
			scanner = new Scanner(fr);
		
			//parsing of file
			while(scanner.hasNext()) {
				String line = scanner.next();
				
				if(contentType == ContentType.INTEGER) {
					Integer number = Integer.parseInt(line);
					list.add(number);
					
				} else if(contentType == ContentType.STRING) {
					list.add(line);
					
				}
				
				if(Thread.currentThread().isInterrupted())
					throw new InterruptedException("Incorrect completion of file parsing " + file.getName());
				
			}
			
		}catch(IOException e) {
			throw new IOException("Could not read file " + file.getName());
		
		}catch (InterruptedException e) {
			throw e;
			
		}catch (Exception e) {
			
			String type = "";
			
			if(contentType == ContentType.INTEGER)
				type = "type number Integer";
			else if(contentType == ContentType.STRING)
				type = "String";
			
			throw new IllegalArgumentException("Error in the elements of the file " + file.getName() 
				+ ". Item type must be " + type);
			
		} finally {
			if(scanner != null)
				scanner.close();
			
			if(fr != null)
				fr.close();
			
		}
		
		return list;
	}

	/**
	 * <p>Writing a list of items to a file</p>
	 * The file to write is created in the directoryName directory..
	 * The file name consists of prefixToFileName + sufixToFileName.
	 * 
	 * @param items items to write to file
	 * @param directoryName path to the directory in which the file will be created
	 * @param prefixToFileName file name prefix
	 * @param sufixToFileName file name suffix
	 */
	@Override
	public void writeInFile(List<Object> items, String directoryName, String prefixToFileName, String sufixToFileName) throws IOException {
		File file = new File(directoryName, prefixToFileName + sufixToFileName);
		
		System.out.println("Write to file " + file.getName());
		
		try {
			PrintWriter pw = new PrintWriter(file);
			
			try {
				for(Object item : items) {
					pw.println(item);
					
					if(Thread.currentThread().isInterrupted())
						throw new InterruptedException("File write stream " + prefixToFileName + sufixToFileName 
								+ "interrupted.");
				}
				
			}catch(Exception e) {
				if(e.getMessage() != null)
					throw new IOException(e.getMessage());
				else 
					throw new IOException("Error writing to file " + prefixToFileName + sufixToFileName);
				
			}finally {
				pw.close();
			}
				
		} catch (FileNotFoundException e) {
			throw new IOException("Error writing to file " + prefixToFileName + sufixToFileName);
		}
		
	}
}
