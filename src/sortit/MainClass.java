package sortit;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import sortit.impl.FileEditorImpl;
import sortit.impl.InstructionMakerImpl;
import sortit.interfaces.IFileEditor;
import sortit.interfaces.IInstructionMaker;

public class MainClass {
	public static void main(String[] args) {
		
		try {
			
			//String[] myArgs = {"C:\\example" , "--out-prefix=sorted_", "--content-type=i", "--sort-mode=d"};
			
			//parsing input arguments
			IInstructionMaker instructionMaker = new InstructionMakerImpl();
			SortingInstruction instruction = instructionMaker.getInstruction(args);
			
			//getting files from a directory
			IFileEditor fileEditor = new FileEditorImpl();
			File[] files = fileEditor.getFileElements(instruction.getPath());
			
			List<Thread> threads = new ArrayList<Thread>();
			
			for(File file : files) {
				//thread creation
				ElementForSort elementForSort = new ElementForSort(file, instruction);
				
				Thread thread = new Thread(elementForSort);
				threads.add(thread);
				
				thread.start();
			}
			
			for(Thread thread : threads) {
				//attempt to join threads (try 3 seconds)
				thread.join(3 * 1000);
				
				if(thread.isAlive()) {
					//if the attachment did not happen (thread is alive), then we complete it
					thread.interrupt();
					
					//join the stream
					thread.join();
				}
				
			}
			
			System.out.println("The program has completed");
			
		}catch (Exception e) {
			System.out.println(e.getMessage());
			
			if(e.getMessage() == null) {
				System.out.println("Unknown error");
			}
		}
		
	}

}
