package sortit.interfaces;

import sortit.SortingInstruction;

public interface IInstructionMaker {
	public SortingInstruction getInstruction(String[] args) throws IllegalArgumentException;

}
