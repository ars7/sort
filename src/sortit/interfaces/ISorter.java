package sortit.interfaces;

import java.util.List;

import sortit.enums.ContentType;
import sortit.enums.SortingDirection;

public interface ISorter {
	public void sortWithChangeList(List<Object> list, ContentType contentType, SortingDirection sortingDirection, String fileName) throws ClassCastException, InterruptedException;
	
}
