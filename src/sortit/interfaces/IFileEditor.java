package sortit.interfaces;

import java.io.File;
import java.io.IOException;
import java.util.List;

import sortit.enums.ContentType;

public interface IFileEditor {
	public File[] getFileElements(String fileDirectory) throws IOException;
	
	public List<Object> listItemsFromFile(File file, ContentType contentType) throws IOException, IllegalArgumentException, InterruptedException;
	
	public void writeInFile(List<Object> items, String directoryName, String prefixToFileName, String sufixToFileName) throws IOException;
	
}
